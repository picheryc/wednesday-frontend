import './App.css';
import {BarreApplication} from "./components/BarreApplication";
import {Acceuil} from "./components/Acceuil";
import {Provider} from "react-redux";
import {initStore} from "./store/initStore";
import {getOrders} from "./store/domain/commande/reducer";


const App = () => {
    const store = initStore();
    setInterval(() => store.dispatch(getOrders()), 10000);

    return (
        <Provider store={store}>
            <div className="App">
                <header className="App-header">
                    <BarreApplication/>
                    <Acceuil/>
                </header>
            </div>
        </Provider>
    );
};

export default App;
