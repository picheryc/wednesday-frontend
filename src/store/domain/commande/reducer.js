export const ORDERS_RECEIVED = "ORDERS_RECEIVED";
export const ORDER_RECEIVED = "ORDER_RECEIVED";
export const DISHES_RECEIVED = "DISHES_RECEIVED";
export const TABLES_RECEIVED = "TABLES_RECEIVED";
export const UNSELECT_ORDER = "UNSELECT_ORDER";

export const getOrders = () => (dispatch, getState, {httpClient}) =>
    httpClient.get("/orders").then(data => dispatch(createAction(ORDERS_RECEIVED, data)));

export const getOrder = (id) => (dispatch, getState, {httpClient}) =>
    httpClient.get(`/orders/${id}`).then(data => dispatch(createAction(ORDER_RECEIVED, data)));

export const getDishes = () => (dispatch, getState, {httpClient}) =>
    httpClient.get("/dishes").then(data => dispatch(createAction(DISHES_RECEIVED, data)));

export const getTables = () => (dispatch, getState, {httpClient}) =>
    httpClient.get("/restaurant/tables").then(data => dispatch(createAction(TABLES_RECEIVED, data)));

export const addDish = (dishId) => (dispatch, getState, {httpClient}) => {
    const order = orderSelector(getState());
    httpClient.post(`/orders/${order.orderId}/dishes`, {
        id: dishId,
    });
}

export const addTable = (tableId) => (dispatch, getState, {httpClient}) => {
    const order = orderSelector(getState());
    httpClient.post(`/orders/${order.orderId}/table`, {
        id: tableId,
    });
}

export const createOrder = () => (dispatch, getState, {httpClient}) =>
    httpClient.post("/orders").then(order => {
        console.log(order.data["orderId"])
        dispatch(getOrders())
        dispatch(getOrder(order.data));
    });

export const unselectOrder = () => (dispatch) => dispatch(createAction(UNSELECT_ORDER))

export const deleteOrder = (orderId) => (dispatch, getState, {httpClient}) =>
    httpClient.delete(`/orders/${orderId}`);

const createAction = (type, data) => ({type, data: data});

export const reducer = (state, action) => {
    switch (action.type) {
        case ORDERS_RECEIVED: {
            return {...state, orders: action.data};
        }
        case ORDER_RECEIVED: {
            return {...state, order: action.data};
        }
        case DISHES_RECEIVED: {
            return {...state, dishes: action.data};
        }
        case TABLES_RECEIVED: {
            return {...state, tables: action.data};
        }
        case UNSELECT_ORDER: {
            return {...state, order: undefined};
        }
        default:
            return state;
    }
};

export const ordersSelector = state => state.orders;
export const orderSelector = state => state.order;
export const dishesSelector = state => state.dishes;
export const tablesSelector = state => state.tables;
