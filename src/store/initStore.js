import {applyMiddleware, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {axiosHttpClient} from "./dependencies/axiosHttpClient";
import {reducer} from "./domain/commande/reducer";

export const initStore = () => {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const initialState = {
        orders: [],
        dishes: [],
        order: undefined,
    };

    return createStore(
        reducer,
        initialState,
        composeEnhancers(applyMiddleware(thunk.withExtraArgument({httpClient: axiosHttpClient})))
    );
};
