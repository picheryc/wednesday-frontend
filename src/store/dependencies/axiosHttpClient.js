import axios from "axios";

const baseHttpClient = axios.create({baseURL: process.env.REACT_APP_SERVER_BASE_URL});
export const axiosHttpClient = {
    get: url =>
        baseHttpClient
            .get(url, {
                headers: {
                    Accept: "application/json; charset=utf-8",
                    "Access-Control-Allow-Origin": "*"
                },
            })
            .then(({data}) => data),
    post: (url, data) => baseHttpClient.post(url, data, {
        headers: {
            Accept: "application/json; charset=utf-8",
            "Access-Control-Allow-Origin": "*",
            "Content-type": "application/json"
        }}),
    delete: (url) => baseHttpClient.delete(url),
};
