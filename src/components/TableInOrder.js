import React from "react";
import {Card, CardContent, Grid, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        marginBottom: 10
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 40,
    },
    pos: {
        marginBottom: 12,
    },
});

export const TableInOrder = ({table}) => {
    const classes = useStyles();
    return <Grid item xs={2} style={{padding: 20}}>
        <Card className={classes.root}>
            <CardContent>
                <Grid container>
                    <Grid item xs={6}>
                        <img src={"https://img.icons8.com/ios/452/restaurant-table.png"} height={100}/>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography variant="h7" className={classes.title}>
                            {table}
                        </Typography>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </Grid>
}

