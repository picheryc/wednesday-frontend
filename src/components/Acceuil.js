import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";
import React from "react";
import OrdersPreview from "./preview/OrdersPreview";
import ModifierCommande from "./EditOrder";

const useStyles = makeStyles(theme => ({
    content: {
        top: 70,
        width: "100%",
        flexGrow: 1,
        position: "relative",
    },
    detail: {
        padding: 20,
    },
}));

export const Acceuil = () => {
    const classes = useStyles();
    return (
        <Grid container className={classes.content}>
            <Grid item xs={8} className={classes.detail}>
                <ModifierCommande/>
            </Grid>
            <Grid item xs={4}>
                <OrdersPreview/>
            </Grid>
        </Grid>
    );
};
