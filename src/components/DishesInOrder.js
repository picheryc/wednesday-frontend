import React from "react";
import {Grid, Typography} from "@material-ui/core";
import {DishInOrder} from "./DishInOrder";

export const DishesInOrder = ({orderDishes, dishes}) => {
    return <Grid container>
        {orderDishes?.length > 0 && <Typography style={{paddingTop: 50}}>
            Plats de la commande
        </Typography>}
        <Grid item xs={12}>
            <Grid container>
                {orderDishes?.map(orderDish => <DishInOrder orderDish={orderDish}
                                                            dish={dishes.find(dish => dish.id === orderDish.id)}/>)}
            </Grid>
        </Grid>
    </Grid>
};