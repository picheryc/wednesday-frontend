import React from "react";
import {Card, CardContent, Grid, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {connect} from "react-redux";
import {addTable} from "../store/domain/commande/reducer";

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        marginBottom: 10
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 40,
    },
    pos: {
        marginBottom: 12,
    },
});

const Table = ({table, addTable, setClosed}) => {
    const classes = useStyles();
    return <Grid item xs={2} style={{padding: 20}} onClick={() => {
        addTable(table)
        setClosed();
    }}>
        <Card className={classes.root}>
            <CardContent>
                <Grid container>
                    <Grid item xs={6}>
                        <img src={"https://img.icons8.com/ios/452/restaurant-table.png"} height={100}/>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography variant="h7" className={classes.title}>
                            {table}
                        </Typography>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </Grid>
}

const mapStateToProps = state => undefined

const mapDispatchToProps = dispatch => ({
    addTable: (id) => dispatch(addTable(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Table)


