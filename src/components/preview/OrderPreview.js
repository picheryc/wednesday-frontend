import React, {useEffect} from "react";
import {Card, CardContent, Grid, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import {dishesSelector, getDishes, getOrder} from "../../store/domain/commande/reducer";
import {connect} from "react-redux";

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        marginBottom: 10
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 20,
    },
    pos: {
        marginBottom: 12,
    },
});

const getTimeDiff = timestamp => Math.round((Date.now() - timestamp) / 1000 / 60);


const OrderPreview = ({order, getOrder, getDishes}) => {
    const classes = useStyles();
    useEffect(getDishes, [])
    return <Grid item xs={12}>
        <Card className={classes.root} onClick={() => getOrder(order.orderId)}>
            <CardContent>
                <Grid container>
                    <Grid item xs={5}>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Commande {order.orderId}
                        </Typography>
                    </Grid>
                    <Grid item xs={3}>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            {order.table === null && "Pas de table"}
                            {order.table !== null && "Table " + order.table?.id}
                        </Typography>
                    </Grid>
                    <Grid item xs={4}>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            <AccessTimeIcon style={{height: 15}}/>il y
                            a {getTimeDiff(parseInt(order.createdAt))} mins
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Total: {order.amount} €
                        </Typography>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    </Grid>
}


const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => ({
    getOrder: (id) => dispatch(getOrder(id)),
    getDishes: () => dispatch(getDishes()),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderPreview)


