import {createOrder, getOrder, getOrders, ordersSelector} from "../../store/domain/commande/reducer";
import React, {useEffect} from "react";
import {connect} from "react-redux";
import {Button, Grid, Typography} from "@material-ui/core";
import AddIcon from '@material-ui/icons/Add';
import OrderPreview from "./OrderPreview";

const OrdersPreview = ({orders, getOrders, createOrder}) => {
    useEffect(() => getOrders(), []);
    return <Grid container>
        <Grid item xs={12}>
            <Typography variant="h4">Commandes</Typography>
        </Grid>
        {orders.sort((a, b) => parseInt(a.createdAt) - parseInt(b.createdAt)).map(order => (
            <OrderPreview order={order}/>))}
        <Grid item xs={12}>
            <Button variant="outlined" color="secondary" onClick={() => {
                createOrder();
                setTimeout(getOrders, 500);
            }}><AddIcon/></Button>
        </Grid>
    </Grid>
};

const mapStateToProps = state => ({
    orders: ordersSelector(state)
})

const mapDispatchToProps = dispatch => ({
    getOrders: () => dispatch(getOrders()),
    createOrder: () => dispatch(createOrder())
});

export default connect(mapStateToProps, mapDispatchToProps)(OrdersPreview)



