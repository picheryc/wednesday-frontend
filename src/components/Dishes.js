import React, {useEffect} from "react";
import {connect} from "react-redux";
import {Grid, Typography} from "@material-ui/core";
import {dishesSelector, getDishes} from "../store/domain/commande/reducer";
import Dish from "./Dish";

const Dishes = ({dishes, getDishes, setClosed}) => {
    useEffect(() => getDishes(), []);
    return <Grid container>
        <Grid item xs={12}>
            <Typography variant="h4">Plats</Typography>
            <Grid container>
                {dishes?.map(dish => <Dish dish={dish} setClosed={setClosed}/>)}
            </Grid>
        </Grid>
    </Grid>
};

const mapStateToProps = state => ({
    dishes: dishesSelector(state)
})

const mapDispatchToProps = dispatch => ({
    getDishes: () => dispatch(getDishes()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dishes)



