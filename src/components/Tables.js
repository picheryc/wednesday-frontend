import React, {useEffect} from "react";
import {connect} from "react-redux";
import {Grid, Typography} from "@material-ui/core";
import {getTables, tablesSelector} from "../store/domain/commande/reducer";
import Table from "./Table";

const Tables = ({tables, getTables, setClosed}) => {
    useEffect(getTables, []);
    return <Grid container>
        <Grid item xs={12}>
            <Typography variant="h4">Plats</Typography>
            <Grid container>
                {tables?.map(table => <Table table={table.id} setClosed={setClosed}/>)}
            </Grid>
        </Grid>
    </Grid>
};

const mapStateToProps = state => ({
    tables: tablesSelector(state)
})

const mapDispatchToProps = dispatch => ({
    getTables: () => dispatch(getTables()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Tables)



