import React, {useEffect, useState} from "react";
import {Button, Card, CardActions, CardContent, Grid, Modal, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {
    deleteOrder,
    dishesSelector,
    getDishes,
    getOrder,
    getOrders,
    orderSelector,
    unselectOrder
} from "../store/domain/commande/reducer";
import {connect} from "react-redux";
import Dishes from "./Dishes";
import {DishesInOrder} from "./DishesInOrder";
import Tables from "./Tables";
import {TableInOrder} from "./TableInOrder";

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        marginBottom: 10
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

const EditOrder = ({order, dishes, deleteOrder, unselectOrder, getOrders, getOrder, getDishes}) => {
    useEffect(getDishes, [])
    const classes = useStyles();
    const [dishSelectionOpen, setDishSelectionOpen] = useState(false);
    const [tableSelectionOpen, setTableSelectionOpen] = useState(false);
    if (order === undefined || order === null) return <div/>
    return <Grid item xs={12}>
        <Typography variant="h4" gutterBottom>
            Editer la commande
        </Typography>
        <Card className={classes.root}>
            <CardContent>
                <Grid container>
                    <Grid item xs={10}>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Commande numéro {order.orderId}
                        </Typography>
                    </Grid>
                    <Grid item xs={2} onClick={unselectOrder}>
                        <Button>Fermer</Button>
                    </Grid>
                </Grid>
                {order.table === null && <Button onClick={() => setTableSelectionOpen(true)}>Choisir une table</Button>}
                {order.table !== null && <TableInOrder table={order.table.id}/>}
                <DishesInOrder orderDishes={order.dishes} dishes={dishes}/>
                <Typography style={{paddingTop: 50}}>
                    Total: {order.amount} €
                </Typography>
            </CardContent>
            <CardActions style={{position: "relative"}}>
                <Button style={{width: "100%"}} onClick={() => setDishSelectionOpen(true)}>Ajouter un plat</Button>
                <Button style={{width: "100%"}} onClick={() => {
                    unselectOrder();
                    deleteOrder(order.orderId);
                    setInterval(getOrders, 500);
                }}>Supprimer la
                    commande</Button>
            </CardActions>
        </Card>
        <Modal
            open={dishSelectionOpen}
            onClose={() => setDishSelectionOpen(false)}
            children={<Dishes setClosed={() => {
                setDishSelectionOpen(false);
                setTimeout(() => getOrder(order.orderId), 500);
            }}/>}
        />
        <Modal
            open={tableSelectionOpen}
            onClose={() => setTableSelectionOpen(false)}
            children={<Tables setClosed={() => {
                setTableSelectionOpen(false);
                setTimeout(() => getOrder(order.orderId), 500);
            }}/>}
        />
    </Grid>
}

const mapStateToProps = state => ({
    order: orderSelector(state),
    dishes: dishesSelector(state)
})

const mapDispatchToProps = dispatch => ({
    unselectOrder: () => dispatch(unselectOrder()),
    deleteOrder: (id) => dispatch(deleteOrder(id)),
    getOrders: () => dispatch(getOrders()),
    getOrder: (id) => dispatch(getOrder(id)),
    getDishes: () => dispatch(getDishes())
});

export default connect(mapStateToProps, mapDispatchToProps)(EditOrder)


