import AppBar from "@material-ui/core/AppBar";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import React from "react";
import logo from "./images/logo.png"

const useStyles = makeStyles(theme => ({
    root: {
        position: "absolute",
        top: 0,
        width: "100%",
    },
    title: {
        flexGrow: 1,
    },
    link: {
        color: "inherit",
        textDecoration: "inherit",
    },
    logo: {
        height: 40,
        paddingLeft: 3,
        paddingTop: 5,
    },
    appbar: {
        backgroundColor: "#111111"
    },
}));


export const BarreApplication = ({panier}) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static" className={classes.appbar}>
                <Toolbar>
                    <Grid container direction="raw">
                        <Grid item xs={2} style={{textAlign: "left"}}>
                            <img src={logo} className={classes.logo}/>
                        </Grid>
                        <Grid item xs={10} style={{textAlign: "left"}}>
                            Wednesday
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
        </div>
    );
};